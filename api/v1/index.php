<?php
require '.././../wp-blog-header.php';
require_once '../include/DbHandler.php';
require_once '../include/CategoriesDbHandler.php';
require_once '../include/PostsDbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader ();

$app = new \Slim\Slim ();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
	// Getting request headers
	$headers = apache_request_headers ();
	$response = array ();
	$app = \Slim\Slim::getInstance ();
	
	
	// Verifying Authorization Header
	if (isset ( $headers ['X-Authorization'] )) {
		$db = new DbHandler ();
		
		// get the api key
		$api_key = $headers ['X-Authorization'];
		// validating api key
		if (! $db->isValidApiKey ( $api_key )) {
			$response ["error"] = true;
			$response ["message"] = "Access Denied. Invalid Api key";
			echoRespnse ( 401, $response );
			$app->stop ();
		} else {
			global $user_id;
			$user_id = $db->getUserId ( $api_key );
		}
	} else {
		$response ["error"] = true;
		$response ["message"] = "Api key is misssing";
		echoRespnse ( 400, $response );
		$app->stop ();
	}
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post ( '/register', function () use($app) {
	// check for required params
	verifyRequiredParams ( array (
			'username',
			'email',
			'password' 
	) );
	
	$response = array ();
	
	// reading post params
	$username = $app->request->post ( 'username' );
	$email = $app->request->post ( 'email' );
	$password = $app->request->post ( 'password' );
	
	// validating email address
	validateEmail ( $email );
	$db = new DbHandler ();
	$hash_password = wp_hash_password ( $password );
	$res = $db->createUser ( $username, $email, $hash_password );
	$status_code = 201;
	if ($res == USER_CREATED_SUCCESSFULLY) {
		$userInfo = get_user_by( 'email', $email );
		wp_new_user_notification($userInfo -> ID, $password);
		$response ["error"] = false;
		$response ["message"] = "You are successfully registered";
	} else if ($res == USER_CREATE_FAILED) {
		$response ["error"] = true;
		$response ["message"] = "Oops! An error occurred while registereing";
		$status_code = 400;
	} else if ($res == USERNAME_ALREADY_EXISTED) {
		$response ["error"] = true;
		$response ["message"] = "Sorry, this username already existed";
		$status_code = 409;
	} else if ($res == EMAIL_ALREADY_EXISTED) {
		$response ["error"] = true;
		$response ["message"] = "Sorry, this email already existed";
		$status_code = 409;
	}
	echoRespnse ( $status_code, $response );
} );



	$app->post ( '/loginWithSocial', function () use($app) {
		// check for required params
		verifyRequiredParams ( array (
		'username',
		'email',
		'password'
				) );
	
		$response = array ();
	
		// reading post params
		$username = $app->request->post ( 'username' );
		$email = $app->request->post ( 'email' );
		$password = $app->request->post ( 'password' );
	
		// validating email address
		validateEmail ( $email );
		$db = new DbHandler ();
		$hash_password = wp_hash_password ( $password );
		$res = $db->createUser ( $username, $email, $hash_password );
		$status_code = 201;
		if ($res == USER_CREATED_SUCCESSFULLY) {
			$userInfo = get_user_by( 'email', $email );
			wp_new_user_notification($userInfo -> ID, $password);
			$app_token = $db->getAppTokenByUserName ( $username );
			
			if ($app_token ['access_token'] == NULL) {
				$db->createToken ( $username );
				$app_token = $db->getAppTokenByUserName ( $username );
			}
				
			$response ["error"] = false;
			$data = array ();
			$data ['access_token'] = $app_token ['access_token'];
			$data ['time_last_login'] = $app_token ['time_last_login'];
			$data ['user'] = $userInfo->data;
			$response ['data'] = $data;
			
			
			
		} else if ($res == USER_CREATE_FAILED) {
			$response ["error"] = true;
			$response ["message"] = "Oops! An error occurred while registereing";
			$status_code = 400;
		} else if ($res == USERNAME_ALREADY_EXISTED) {
			$status_code = 200;
			$user = $db->getUserByUserName ( $username );
			$app_token = $db->getAppTokenByUserName ( $username );
				
			if ($app_token ['access_token'] == NULL) {
				$db->createToken ( $username );
				$app_token = $db->getAppTokenByUserName ( $username );
			}
			
			$response ["error"] = false;
			$data = array ();
			$data ['access_token'] = $app_token ['access_token'];
			$data ['time_last_login'] = $app_token ['time_last_login'];
			$data ['user'] = $user;
			$response ['data'] = $data;
			
		} else if ($res == EMAIL_ALREADY_EXISTED) {
			$status_code = 200;
			$userInfo = get_user_by( 'email', $email );
			$app_token = $db->getAppTokenByUserName ( $username );
				
			if ($app_token ['access_token'] == NULL) {
				$db->createToken ( $username );
				$app_token = $db->getAppTokenByUserName ( $username );
			}
			
			$response ["error"] = false;
			$data = array ();
			$data ['access_token'] = $app_token ['access_token'];
			$data ['time_last_login'] = $app_token ['time_last_login'];
			$data ['user'] = $userInfo->data;
			$response ['data'] = $data;

		}
		echoRespnse ( $status_code, $response );
	} );





/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post ( '/login', function () use($app) {
	// check for required params
	verifyRequiredParams ( array (
			'username',
			'password' 
	) );
	
	// reading post params
	$username = $app->request ()->post ( 'username' );
	$password = $app->request ()->post ( 'password' );
	$response = array ();
	
	$db = new DbHandler ();
	
	// check for correct username and password
	if ($db->checkLogin ( $username, $password )) {
		// get the user by username
		$user = $db->getUserByUserName ( $username );
		$app_token = $db->getAppTokenByUserName ( $username );
		if ($user != NULL) {
			if ($app_token ['access_token'] == NULL) {
				$db->createToken ( $username );
				$app_token = $db->getAppTokenByUserName ( $username );
			}
			
			$response ["error"] = false;
			$data = array ();
			$data ['access_token'] = $app_token ['access_token'];
			$data ['time_last_login'] = $app_token ['time_last_login'];
			$data ['user'] = $user;
			$response ['data'] = $data;
		} else {
			// unknown error occurred
			$response ['error'] = true;
			$response ['message'] = "An error occurred. Please try again";
		}
	} else {
		// user credentials are wrong
		$response ['error'] = true;
		$response ['message'] = 'Login failed. Incorrect credentials';
	}
	
	echoRespnse ( 200, $response );
} );

/*
 * ----------- METHODS CATEGORIES ---------------------------------
 */
/**
 * /**
 * Get Categories
 * url - /categories
 * method - GET
 */
$app->get ( '/categories', function () {
	$response = array ();
	$db = new CategoriesDbHandler ();
	$result = $db->getAllCategories ();
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
} );

/**
 * Get Category
 * url /tasks/:id
 * method - GET
 * Will return 404 if the Category not found
 */
$app->get ( '/categories/:id', function ($category_id) {
	$response = array ();
	$db = new CategoriesDbHandler ();
	$result = $db->getCategoryById ( $category_id );
	if ($result != NULL) {
		$response ["error"] = false;
		$response ["data"] = $result;
		echoRespnse ( 200, $response );
	} else {
		$response ["error"] = true;
		$response ["message"] = "The requested resource doesn't exists";
		echoRespnse ( 404, $response );
	}
} );

/*
 * --------------------POST----------------------------------------
 */
$app->get ( '/posts/:category_id', function ($category_id) {
	$response = array ();
	$db = new PostsDbHandler ();
	$result = $db->getPostByCategory ( $category_id );
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
} );

/*
 * -------------------------UPLOAD--------------------------
 */
$app->post ( '/upload', function () {
	$response = array ();
	if (isset ( $_FILES ['file'] )) {

		ini_set("post_max_size", "200M");
    	ini_set("upload_max_filesize", "200M");
    	ini_set("memory_limit", "20000M"); 

		$file = $_FILES ['file'];
		$file_name = $file ['name'];
		$file_tmp = $file ['tmp_name'];
		$file_size = $file ['size'];
		$file_error = $file ['error'];
		
		$root_folder_uploads = ".././../wp-content/uploads";
		
		$network_link_uploads_file = site_url () . '/wp-content/uploads';
		$path_link_uploads = '/wp-content/uploads';
		
		$file_ext = explode ( '.', $file_name );
		$file_ext = strtolower ( end ( $file_ext ) );
		
		$allowed = array (
				'jpg',
				'png',
				'mp4',
				'flv' 
		);
		if (in_array ( $file_ext, $allowed )) {
			if ($file_error === 0) {
				if ($file_size <= 1073741824) {
					$file_name_new = uniqid ( '', true ) . '.' . $file_ext;
					$root_folder_uploads_year = $root_folder_uploads . '/' . date ( "Y" );
					$network_link_uploads_file = $network_link_uploads_file . '/' . date ( "Y" );
					$path_link_uploads = $path_link_uploads . '/' . date ( "Y" );
					if (! file_exists ( $root_folder_uploads_year )) {
						if (! mkdir ( $root_folder_uploads_year, 0777, true )) {
							$response ["error"] = true;
							$response ["message"] = "Failed to create folders year";
							echoRespnse ( 400, $response );
							exit ();
						}
					}
					$root_folder_uploads_month = $root_folder_uploads_year . '/' . date ( 'm' );
					$network_link_uploads_file = $network_link_uploads_file . '/' . date ( 'm' );
					$path_link_uploads = $path_link_uploads . '/' . date ( 'm' );
					if (! file_exists ( $root_folder_uploads_month )) {
						if (! mkdir ( $root_folder_uploads_month, 0777, true )) {
							$response ["error"] = true;
							$response ["message"] = "Failed to create folders month";
							echoRespnse ( 400, $response );
							exit ();
						}
					}
					$file_destination = $root_folder_uploads_month . '/' . $file_name_new;
					$network_link_uploads_file = $network_link_uploads_file . '/' . $file_name_new;
					$path_link_uploads = $path_link_uploads . '/' . $file_name_new;
					if (move_uploaded_file ( $file_tmp, $file_destination )) {
						$response ["error"] = false;
						$response ["link"] = $network_link_uploads_file;
						$response ["path"] = $path_link_uploads;
						echoRespnse ( 200, $response );
					} else {
						$response ["error"] = true;
						$response ["message"] = "Failed to move uploaded file";
						echoRespnse ( 400, $response );
					}
				} else {
					$response ["error"] = true;
					$response ["message"] = "File big too";
					echoRespnse ( 400, $response );
				}
			}
		} else {
			$response ["error"] = true;
			$response ["message"] = "File extension not allowed";
			echoRespnse ( 400, $response );
		}
	} else {
		$response ["error"] = true;
		$response ["message"] = "File not found";
		echoRespnse ( 404, $response );
	}
} );

$app->post ( '/addNew', function () {
	$postVideo = array (
			'ID' => false,
			'post_content' => 'POST CONTENT',
			'post_title' => 'post  title',
			'post_status' => 'publish',
			'post_type' => 'video_listing',
			'post_author' => '1001025',
			'ping_status' => 'open',
			'menu_order' => 0, // If new post is a page, sets the order in which it should appear in supported menus. Default 0.
			'comment_status' => 'open' 
	) // Default is the option 'default_comment_status', or 'closed'.
;
	$postVideoid = wp_insert_post ( $postVideo );
	
	$filename = '/wp-content/uploads/2015/07/55a3f2c1e149c9.30366773.jpg';
	$filenamefull = 'http://localhost/UploadVideoWebsite/wp-content/uploads/2015/07/55a3f2c1e149c9.30366773.jpg';
	$filetype = wp_check_filetype ( basename ( $filename ), null );
	
	$attachment = array (
			'ID' => false,
			'post_status' => 'publish',
			'post_mime_type' => $filetype ['type'],
			'post_title' => '7',
			'post_content' => '',
			'post_status' => 'inherit',
			'post_author' => '1001025',
			'menu_order' => 1,
			'comment_status' => 'open' 
	);
	
	$cat_id = wp_insert_term ( 'Demo Ahs Tags', 'video_tag', array (
			'description' => '',
			'slug' => sanitize_title ( 'Demo Ah' ) 
	) );
	
	wp_set_object_terms ( $postVideoid, $cat_id ['term_id'], 'video_tag' );
	
	wp_set_object_terms ( $postVideoid, 34, 'video_cat' );
	
	wp_update_term_count ( $cat_id ['term_id'], 'video_tag' );
	wp_update_term_count ( 34, 'video_cat' );
	
	$attach_id = wp_insert_attachment ( $attachment, $filename, $postVideoid );
	
	require_once ('.././../wp-admin/includes/image.php');
	
	update_post_meta ( $postVideoid, '_post_title', '' );
	update_post_meta ( $postVideoid, '_post_content', '' );
	add_post_meta ( $postVideoid, '_video_url', '' );
	add_post_meta ( $postVideoid, '_meta_video', 'http://localhosts/UploadVideoWebsite/wp-content/uploads/2015/07/55a40280b02db2.80415627.mp4' );
	add_post_meta ( $postVideoid, '_meta_image', $filenamefull );
	add_post_meta ( $postVideoid, '_meta_zipcode', '' );
	add_post_meta ( $postVideoid, '_meta_keywords', '' );
	add_post_meta ( $postVideoid, '_meta_address', '' );
	add_post_meta ( $postVideoid, '_meta_country', '' );
	add_post_meta ( $postVideoid, '_meta_city', '' );
	
	update_attached_file ( $attach_id, 'temp/7.jpg' );
	wp_update_attachment_metadata ( $attach_id, 'a:4:{s:5:"width";i:580;s:6:"height";i:480;s:14:"hwstring_small";s:24:"height=\'100\' width=\'100\'";s:4:"file";s:10:"temp/7.jpg";}' );
	return $result;
} );

$app->get ( '/getMyVideo/:user_id','authenticate', function ($user_id) {
	$response = array ();
	$db = new PostsDbHandler ();
	$result = $db->getMyVideo($user_id);
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
} );


$app->post ( '/uploadVideo/:user_id','authenticate', function ($user_id) use($app) {
	verifyRequiredParams ( array (
			'video_title',
			'description',
			'tags',
			'address',
			'city',
			'country',
			'category'
	) );
	
	$infoVideo = array();
	$infoVideo ['videourl'] = $app->request->post ( 'video_url' );
	$infoVideo ['place_image1'] = $app->request->post ( 'video_meta' );
	$infoVideo ['place_image2'] = $app->request->post ( 'image_meta' );
	$infoVideo ['zipcode'] = $app->request->post ( 'zipcode' );
	$infoVideo ['keywords'] = $app->request->post ( 'keywords' );
	$infoVideo ['address'] = $app->request->post ( 'address' );
	$infoVideo ['country'] = $app->request->post ( 'country' );
	$infoVideo ['city'] = $app->request->post ( 'city' );
	
	$infoVideo ['place_title'] = $app->request->post ( 'video_title' );
	$infoVideo ['description'] = $app->request->post ( 'description' );
	$infoVideo ['category'] = $app->request->post ( 'category' );
	$infoVideo ['place_tag'] = $app->request->post ( 'tags' );
	
	
	$db = new PostsDbHandler ();
	$result = $db->uploadMyVideo($user_id, $infoVideo);
	$response = array ();
	
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
});


$app->post ( '/deleteVideo/:id_video','authenticate', function ($id_video){
	
	
	$db = new PostsDbHandler ();
	$result = $db->deleteVideo($id_video);
	$response = array ();
	
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
});



$app->post ( '/editVideo/:id_video', 'authenticate', function ($id_video)  use($app){
	
	verifyRequiredParams ( array (
	'place_title',
	'description',
	'place_tag'
			) );
	
	
	$infoVideo = array();
	$infoVideo ['postid'] = $id_video;
	$infoVideo ['place_title'] = $app->request->post ( 'place_title' );
	$infoVideo ['description'] = $app->request->post ( 'description' );
	$infoVideo ['_video_url'] = $app->request->post ( '_video_url' );
	$infoVideo ['_meta_image'] = $app->request->post ( '_meta_image' );
	$infoVideo ['_meta_video'] = $app->request->post ( '_meta_video' );
	$infoVideo ['place_tag'] = $app->request->post ( 'place_tag' );
	
	
	
	
	
	$db = new PostsDbHandler ();
	$result = $db->editVideo($infoVideo);
	$response = array ();
	
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
	
	
	
	
} );



$app->get ( '/searchByWhatWhere', function () {
	
	$what = $_GET['what'];
	$where = $_GET['where'];
	

	$response = array ();
	$db = new PostsDbHandler ();
	$result = $db->searchByWhatWhere ( $what, $where );
	$response ["error"] = false;
	$response ["data"] = $result;
	echoRespnse ( 200, $response );
} );









/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks
 */
$app->get ( '/tasks', 'authenticate', function () {
	global $user_id;
	$response = array ();
	$db = new DbHandler ();
	
	// fetching all user tasks
	$result = $db->getAllUserTasks ( $user_id );
	
	$response ["error"] = false;
	$response ["tasks"] = array ();
	
	// looping through result and preparing tasks array
	while ( $task = $result->fetch_assoc () ) {
		$tmp = array ();
		$tmp ["id"] = $task ["id"];
		$tmp ["task"] = $task ["task"];
		$tmp ["status"] = $task ["status"];
		$tmp ["createdAt"] = $task ["created_at"];
		array_push ( $response ["tasks"], $tmp );
	}
	
	echoRespnse ( 200, $response );
} );

/**
 * Listing single task of particual user
 * method GET
 * url /tasks/:id
 * Will return 404 if the task doesn't belongs to user
 */
$app->get ( '/tasks/:id', 'authenticate', function ($task_id) {
	global $user_id;
	$response = array ();
	$db = new DbHandler ();
	
	// fetch task
	$result = $db->getTask ( $task_id, $user_id );
	
	if ($result != NULL) {
		$response ["error"] = false;
		$response ["id"] = $result ["id"];
		$response ["task"] = $result ["task"];
		$response ["status"] = $result ["status"];
		$response ["createdAt"] = $result ["created_at"];
		echoRespnse ( 200, $response );
	} else {
		$response ["error"] = true;
		$response ["message"] = "The requested resource doesn't exists";
		echoRespnse ( 404, $response );
	}
} );

/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /tasks/
 */
$app->post ( '/tasks', 'authenticate', function () use($app) {
	// check for required params
	verifyRequiredParams ( array (
			'task' 
	) );
	
	$response = array ();
	$task = $app->request->post ( 'task' );
	
	global $user_id;
	$db = new DbHandler ();
	
	// creating new task
	$task_id = $db->createTask ( $user_id, $task );
	
	if ($task_id != NULL) {
		$response ["error"] = false;
		$response ["message"] = "Task created successfully";
		$response ["task_id"] = $task_id;
		echoRespnse ( 201, $response );
	} else {
		$response ["error"] = true;
		$response ["message"] = "Failed to create task. Please try again";
		echoRespnse ( 200, $response );
	}
} );

/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 */
$app->put ( '/tasks/:id', 'authenticate', function ($task_id) use($app) {
	// check for required params
	verifyRequiredParams ( array (
			'task',
			'status' 
	) );
	
	global $user_id;
	$task = $app->request->put ( 'task' );
	$status = $app->request->put ( 'status' );
	
	$db = new DbHandler ();
	$response = array ();
	
	// updating task
	$result = $db->updateTask ( $user_id, $task_id, $task, $status );
	if ($result) {
		// task updated successfully
		$response ["error"] = false;
		$response ["message"] = "Task updated successfully";
	} else {
		// task failed to update
		$response ["error"] = true;
		$response ["message"] = "Task failed to update. Please try again!";
	}
	echoRespnse ( 200, $response );
} );

/**
 * Deleting task.
 * Users can delete only their tasks
 * method DELETE
 * url /tasks
 */
$app->delete ( '/tasks/:id', 'authenticate', function ($task_id) use($app) {
	global $user_id;
	
	$db = new DbHandler ();
	$response = array ();
	$result = $db->deleteTask ( $user_id, $task_id );
	if ($result) {
		// task deleted successfully
		$response ["error"] = false;
		$response ["message"] = "Task deleted succesfully";
	} else {
		// task failed to delete
		$response ["error"] = true;
		$response ["message"] = "Task failed to delete. Please try again!";
	}
	echoRespnse ( 200, $response );
} );

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
	$error = false;
	$error_fields = "";
	$request_params = array ();
	$request_params = $_REQUEST;
	// Handling PUT request params
	if ($_SERVER ['REQUEST_METHOD'] == 'PUT') {
		$app = \Slim\Slim::getInstance ();
		parse_str ( $app->request ()->getBody (), $request_params );
	}
	foreach ( $required_fields as $field ) {
		if (! isset ( $request_params [$field] ) || strlen ( trim ( $request_params [$field] ) ) <= 0) {
			$error = true;
			$error_fields .= $field . ', ';
		}
	}
	
	if ($error) {
		// Required field(s) are missing or empty
		// echo error json and stop the app
		$response = array ();
		$app = \Slim\Slim::getInstance ();
		$response ["error"] = true;
		$response ["message"] = 'Required field(s) ' . substr ( $error_fields, 0, - 2 ) . ' is missing or empty';
		echoRespnse ( 400, $response );
		$app->stop ();
	}
}

/**
 * Validating email address
 */
function validateEmail($email) {
	$app = \Slim\Slim::getInstance ();
	if (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) {
		$response ["error"] = true;
		$response ["message"] = 'Email address is not valid';
		echoRespnse ( 400, $response );
		$app->stop ();
	}
}

/**
 * Echoing json response to client
 * 
 * @param String $status_code
 *        	Http response code
 * @param Int $response
 *        	Json response
 */
function echoRespnse($status_code, $response) {
	$app = \Slim\Slim::getInstance ();
	// Http response code
	$app->status ( $status_code );
	
	// setting response content type to json
	$app->contentType ( 'application/json' );
	
	echo json_encode ( $response );
}

$app->run ();
?>