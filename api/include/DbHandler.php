<?php

class DbHandler {
	private $conn;
	function __construct() {
		require_once dirname ( __FILE__ ) . '/DbConnect.php';
		// opening db connection
		$db = new DbConnect ();
		$this->conn = $db->connect ();
	}

	public function createUser($username, $email, $password_hash) {
		require_once 'PassHash.php';
		$response = array ();
		if (! $this->isUserNameExists ( $username )) {
			if (! $this->isEmailExists ( $email )) {
				$api_key = $this->generateApiKey ();
				$stmt = $this->conn->prepare ( "INSERT INTO wp_users(wp_users.user_login, wp_users.user_pass, wp_users.user_nicename, wp_users.user_email, wp_users.user_registered, wp_users.display_name) values(?, ?, ?, ?, NOW(), ?)" );
				$stmt->bind_param ( "sssss", $username, $password_hash, $username, $email, $username );
				$result = $stmt->execute ();
				$stmt->close ();
				if ($result) {
					$stmtToken = $this->conn->prepare ( "INSERT INTO app_token(app_token.user_login, app_token.access_token, app_token.time_last_login) values(?, ?, NOW())" );
					$stmtToken->bind_param ( "ss", $username, $api_key );
					$resultKey = $stmtToken->execute ();
					$stmtToken->close ();
					return USER_CREATED_SUCCESSFULLY;
				} else {
					return USER_CREATE_FAILED;
				}
			} else {
				return EMAIL_ALREADY_EXISTED;
			}
		} else {
			return USERNAME_ALREADY_EXISTED;
		}
		return $response;
	}
	

	public function checkLogin($username, $password) {
		// fetching user by email
		$stmt = $this->conn->prepare ( "SELECT u.user_pass FROM wp_users u WHERE u.user_login = ?" );
		
		$stmt->bind_param ( "s", $username );
		
		$stmt->execute ();
		
		$stmt->bind_result ( $password_hash );
		
		$stmt->store_result ();
		
		if ($stmt->num_rows > 0) {
			// Found user with the email
			// Now verify the password
			
			$stmt->fetch ();
			
			$stmt->close ();
			
			require_once '.././../wp-includes/class-phpass.php';
			$wp_hasher = new PasswordHash ( 8, true );
			
			if ($wp_hasher->CheckPassword ( $password, $password_hash )) {
				$stmtCheckExistToken = $this->conn->prepare ( "SELECT COUNT(appt.id) FROM app_token appt WHERE appt.user_login = ?" );
				$stmtCheckExistToken->bind_param ( "s", $username );
				$stmtCheckExistToken->execute ();
				$stmtCheckExistToken->store_result ();
				if($stmtCheckExistToken->num_rows <= 0){
					$api_key = $this->generateApiKey ();
					$stmtToken = $this->conn->prepare ( "INSERT INTO app_token(app_token.user_login, app_token.access_token, app_token.time_last_login) values(?, ?, NOW())" );
					$stmtToken->bind_param ( "ss", $username, $api_key );
					$stmtToken->fetch ();
					$resultKey = $stmtToken->execute ();
					$stmtToken->close ();
				}
				$stmtCheckExistToken->close ();
				
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			$stmt->close ();
			return FALSE;
		}
	}

	private function isEmailExists($email) {
		$stmt = $this->conn->prepare ( "SELECT id from wp_users WHERE user_email = ?" );
		$stmt->bind_param ( "s", $email );
		$stmt->execute ();
		$stmt->store_result ();
		$num_rows = $stmt->num_rows;
		$stmt->close ();
		return $num_rows > 0;
	}

	private function isUserNameExists($username) {
		$stmt = $this->conn->prepare ( "SELECT id from wp_users WHERE user_login = ?" );
		$stmt->bind_param ( "s", $username );
		$stmt->execute ();
		$stmt->store_result ();
		$num_rows = $stmt->num_rows;
		$stmt->close ();
		return $num_rows > 0;
	}
	

	public function getUserByEmail($email) {
		$stmt = $this->conn->prepare ( "SELECT name, email, api_key, status, created_at FROM users WHERE email = ?" );
		$stmt->bind_param ( "s", $email );
		if ($stmt->execute ()) {
			// $user = $stmt->get_result()->fetch_assoc();
			$stmt->bind_result ( $name, $email, $api_key, $status, $created_at );
			$stmt->fetch ();
			$user = array ();
			$user ["name"] = $name;
			$user ["email"] = $email;
			$user ["api_key"] = $api_key;
			$user ["status"] = $status;
			$user ["created_at"] = $created_at;
			$stmt->close ();
			return $user;
		} else {
			return NULL;
		}
	}
	

	public function getUserByUserName($username) {
		$stmt = $this->conn->prepare ( "SELECT u.* FROM wp_users u WHERE u.user_login = ?" );
		$stmt->bind_param ( "s", $username );
		if ($stmt->execute ()) {
			// $user = $stmt->get_result()->fetch_assoc();
			$stmt->bind_result ( $user_id, $user_login, $user_pass, $user_nicename, $user_email, $user_url, $user_registered, $user_activation_key, $user_status, $display_name );
			$stmt->fetch ();
			$user = array ();
			$user ["user_id"] = $user_id;
			$user ["user_login"] = $user_login;
			$user ["user_pass"] = $user_pass;
			$user ["user_nicename"] = $user_nicename;
			$user ["user_email"] = $user_email;
			$user ["user_url"] = $user_url;
			$user ["user_registered"] = $user_registered;
			$user ["user_activation_key"] = $user_activation_key;
			$user ["user_status"] = $user_status;
			$user ["display_name"] = $display_name;
			$stmt->close ();
			return $user;
		} else {
			return NULL;
		}
	}
	

	public function getAppTokenByUserName($username) {
		$stmt = $this->conn->prepare ( "SELECT a.* FROM app_token a WHERE a.user_login = ?" );
		$stmt->bind_param ( "s", $username );
		if ($stmt->execute ()) {
			// $user = $stmt->get_result()->fetch_assoc();
			$stmt->bind_result ( $token_id, $user_login, $access_token, $time_last_login );
			$stmt->fetch ();
			if ($token_id === NULL) {
				$api_key = $this->generateApiKey ();
				$stmtToken = $this->conn->prepare ( "INSERT INTO app_token(app_token.user_login, app_token.access_token, app_token.time_last_login) values(?, ?, NOW())" );
				$stmtToken->bind_param ( "ss", $username, $api_key );
				$resultKey = $stmtToken->execute ();
				$token_id = $api_key;
				$stmtToken->close ();
			}
			$token = array ();
			$token ["token_id"] = $token_id;
			$token ["user_login"] = $user_login;
			$token ["access_token"] = $access_token;
			$token ["time_last_login"] = $time_last_login;
			$stmt->close ();
			return $token;
		} else {
			return NULL;
		}
	}

	public function createToken($username){
		$api_key = $this->generateApiKey ();
		$stmtToken = $this->conn->prepare ( "INSERT INTO app_token(app_token.user_login, app_token.access_token, app_token.time_last_login) values(?, ?, NOW())" );
		$stmtToken->bind_param ( "ss", $username, $api_key );
		$resultKey = $stmtToken->execute ();
		$token_id = $api_key;
		$stmtToken->close ();
	}
	

	public function getApiKeyById($user_id) {
		$stmt = $this->conn->prepare ( "SELECT api_key FROM users WHERE id = ?" );
		$stmt->bind_param ( "i", $user_id );
		if ($stmt->execute ()) {
			// $api_key = $stmt->get_result()->fetch_assoc();
			// TODO
			$stmt->bind_result ( $api_key );
			$stmt->close ();
			return $api_key;
		} else {
			return NULL;
		}
	}
	

	public function getUserId($api_key) {
		$stmt = $this->conn->prepare ( "SELECT a.ID FROM app_token a WHERE a.access_token = ?" );
		$stmt->bind_param ( "s", $api_key );
		if ($stmt->execute ()) {
			$stmt->bind_result ( $user_id );
			$stmt->fetch ();
			// TODO
			// $user_id = $stmt->get_result()->fetch_assoc();
			$stmt->close ();
			return $user_id;
		} else {
			return NULL;
		}
	}
	

	public function isValidApiKey($api_key) {
		$stmt = $this->conn->prepare ( "SELECT a.ID FROM app_token a WHERE a.access_token = ?" );
		$stmt->bind_param ( "s", $api_key );
		$stmt->execute ();
		$stmt->store_result ();
		$num_rows = $stmt->num_rows;
		$stmt->close ();
		return $num_rows > 0;
	}
	

	private function generateApiKey() {
		return md5 ( uniqid ( rand (), true ) );
	}
	
	
}

?>
