<?php

class CategoriesDbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /**
     * Fetching All Categories
     */
    public function getAllCategories() {
        $stmt = $this->conn->prepare("SELECT wpT.* FROM wp_terms wpT, wp_term_taxonomy wpTT WHERE wpT.term_id = wpTT.term_id AND wpTT.taxonomy ='video_cat' and  wpTT.parent=0  ORDER BY wpT.term_order ASC");
        $categories = array ();
        if ($stmt->execute()) {
        	$stmt-> bind_result($term_id, $name, $slug, $term_group, $term_order);

        	while ($stmt->fetch()) {
        		$tmp = array ();
        		$tmp ["term_id"] = $term_id;
        		$tmp ["name"] = $name;
        		$tmp ["slug"] = $slug;
        		$tmp ["term_group"] = $term_group;
        		$tmp ["term_order"] = $term_order;
        		array_push ( $categories, $tmp );
        	}
        }
        $stmt->close();
        return $categories;
    }


    
    /**
     * Fetching Category by id
     * @param String $category_id id of the Category
     */
    public function getCategoryById($category_id) {
    	$stmt = $this->conn->prepare("SELECT t.* FROM wp_terms t WHERE t.term_id = ?");
    	$stmt->bind_param("i", $category_id);
    	if ($stmt->execute()) {
    		$category = array();
    		$stmt->bind_result($term_id, $name, $slug, $term_group, $term_order);
    		$stmt->fetch();
    		$category["term_id"] = $term_id;
    		$category["name"] = $name;
    		$category["slug"] = $slug;
    		$category["term_group"] = $term_group;
    		$category["term_order"] = $term_order;
    		$stmt->close();
    		return $category;
    	} else {
    		return NULL;
    	}
    }

   

}

?>
