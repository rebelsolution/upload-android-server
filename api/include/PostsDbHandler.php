<?php

class PostsDbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function getPostByCategory($category_id) {
    	$stmt = $this->conn->prepare("SELECT p.* FROM wp_posts p, wp_term_relationships tr WHERE tr.object_id = p.ID AND tr.term_taxonomy_id = ? ORDER BY p.post_date DESC");
    	$stmt->bind_param("i", $category_id);
        $wp_posts = array ();
    	if ($stmt->execute()) {
            $stmt->store_result();
            $stmt-> bind_result($id, $post_author, $post_date, $post_date_gmt, $post_content, $post_title, $post_excerpt, $post_status, $comment_status, $ping_status, $post_password, $post_name, $to_ping, $pinged, $post_modified, $post_modified_gmt, $post_content_filtered, $post_parent, $guid, $menu_order, $post_type, $post_mime_type, $comment_count);
            
            while ($stmt->fetch()) {
                $tmp = array ();
                $tmp ["id"] = $id;
                $tmp ["post_author"] = $post_author;
                $tmp['author_username'] = get_the_author_meta('user_nicename', $post_author);
                $tmp ["post_date"] = $post_date;
                $tmp ["post_date_gmt"] = $post_date_gmt;
                $tmp ["post_content"] = htmlspecialchars($post_content);
                $tmp ["post_title"] = htmlspecialchars($post_title);
                $tmp ["post_excerpt"] = $post_excerpt;
                $tmp ["post_status"] = $post_status;
                $tmp ["comment_status"] = $comment_status;
                $tmp ["ping_status"] = $ping_status;
                $tmp ["post_password"] = $post_password;
                $tmp ["post_name"] = htmlspecialchars($post_name);
                $tmp ["to_ping"] = $to_ping;
                $tmp ["pinged"] = $pinged;
                $tmp ["post_modified"] = $post_modified;
                $tmp ["post_modified_gmt"] = $post_modified_gmt;
                $tmp ["post_content_filtered"] = $post_content_filtered;
                $tmp ["post_parent"] = $post_parent;
                $tmp ["guid"] = $guid;
                $tmp ["menu_order"] = $menu_order;
                $tmp ["post_type"] = $post_type;
                $tmp ["post_mime_type"] = $post_mime_type;
                $tmp ["comment_count"] = $comment_count;


                $post_meta = array ();
            
                $post_meta_result = get_post_meta($id);
                
                $post_meta['_post_title'] = htmlspecialchars($post_meta_result['_post_title'][0]);
                $post_meta['_post_content'] = htmlspecialchars($post_meta_result['_post_content'][0]);
                $post_meta['_video_url'] = $post_meta_result['_video_url'][0];
                $post_meta['_meta_video'] = $post_meta_result['_meta_video'][0];
                $post_meta['_meta_image'] = $post_meta_result['_meta_image'][0];
                $post_meta['_meta_zipcode'] = $post_meta_result['_meta_zipcode'][0];
                $post_meta['_meta_keywords'] = htmlspecialchars($post_meta_result['_meta_keywords'][0]);
                
                $post_meta['_meta_address'] = htmlspecialchars($post_meta_result['_meta_address'][0]);
                $post_meta['_meta_country'] = htmlspecialchars($post_meta_result['_meta_country'][0]);
                $post_meta['_meta_city'] = htmlspecialchars($post_meta_result['_meta_city'][0]);
                $post_meta['post_views_count'] = $post_meta_result['post_views_count'][0];
                $tmp ["post_meta"] =  $post_meta;
                array_push ( $wp_posts, $tmp );
            }
        }
    	$stmt->close();
    	return $wp_posts;
    } 
    
    
    public function getMyVideo($author_id) {
    	$stmt = $this->conn->prepare("SELECT wpP.*  FROM wp_posts wpP WHERE wpP.post_author = ? AND wpP.post_type = 'video_listing' AND ( wpP.post_status = 'publish' OR wpP.post_status = 'pending') ORDER BY  wpP.`ID` DESC");
    	$stmt->bind_param("i", $author_id);
    	$wp_posts = array ();
    	if ($stmt->execute()) {
    		$stmt->store_result();
    		$stmt-> bind_result($id, $post_author, $post_date, $post_date_gmt, $post_content, $post_title, $post_excerpt, $post_status, $comment_status, $ping_status, $post_password, $post_name, $to_ping, $pinged, $post_modified, $post_modified_gmt, $post_content_filtered, $post_parent, $guid, $menu_order, $post_type, $post_mime_type, $comment_count);
    
    		while ($stmt->fetch()) {
    			$tmp = array ();
    			$address = get_post_meta($id, 'geocraft_meta_address', true);
    			$categories = get_the_term_list($id, 'video_cat', '', ' ', '');
    			$tags = get_the_term_list($id, 'video_tag', '', ' ', '');
    			$video_url = get_post_meta($id, '_video_url', true);
    			$video_meta = get_post_meta($id, '_meta_video', true);
    			$image_meta = get_post_meta($id, '_meta_image', true);
    			
    			
    			$tmp['address'] = $address;
    			$tmp['categories'] = preg_replace('/<[^<>]+>/', '', $categories);
    			$tmp['tags'] = preg_replace('/<[^<>]+>/', '', $tags);
    			$tmp['video_meta'] = $video_meta;
    			$tmp['image_meta'] = $image_meta;
    			$tmp['video_url'] = $video_url;
    			
    			
    			$tmp ["id"] = $id;
    			$tmp ["post_author"] = $post_author;
    			
    			$tmp['author_username'] = get_the_author_meta('user_nicename', $post_author);
    			$tmp ["post_date"] = $post_date;
    			$tmp ["post_date_gmt"] = $post_date_gmt;
    			$tmp ["post_content"] = htmlspecialchars($post_content);
    			$tmp ["post_title"] = htmlspecialchars($post_title);
    			$tmp ["post_excerpt"] = $post_excerpt;
    			$tmp ["post_status"] = $post_status;
    			$tmp ["comment_status"] = $comment_status;
    			$tmp ["ping_status"] = $ping_status;
    			$tmp ["post_password"] = $post_password;
    			$tmp ["post_name"] = htmlspecialchars($post_name);
    			$tmp ["to_ping"] = $to_ping;
    			$tmp ["pinged"] = $pinged;
    			$tmp ["post_modified"] = $post_modified;
    			$tmp ["post_modified_gmt"] = $post_modified_gmt;
    			$tmp ["post_content_filtered"] = $post_content_filtered;
    			$tmp ["post_parent"] = $post_parent;
    			$tmp ["guid"] = $guid;
    			$tmp ["menu_order"] = $menu_order;
    			$tmp ["post_type"] = $post_type;
    			$tmp ["post_mime_type"] = $post_mime_type;
    			$tmp ["comment_count"] = $comment_count;
    			
    			
    			
    			array_push ( $wp_posts, $tmp );
    		}
    	}
    	$stmt->close();
    	return $wp_posts;
    }
    
    
    
    public function uploadMyVideo($user_id, $infoVideo) {
    	$result = array();
    	
    	$image_meta = array ();
    	$post_meta = array ();
    	$img4 = $temp_url . 'thumb3.jpg';
    	$image_meta [] = $temp_url . "temp/7.jpg";
    	$post_meta = array (
    			'_video_url' => esc_attr ( $infoVideo ['videourl'] ),
    			'_meta_video' => esc_attr ( $infoVideo ['place_image1'] ),
    			'_meta_image' => esc_attr ( $infoVideo ['place_image2'] ),
    			'_meta_zipcode' => esc_attr ( $infoVideo ['zipcode'] ),
    			'_meta_keywords' => esc_attr ( $infoVideo ['keywords'] ),
    			'_meta_address' => esc_attr ( $infoVideo ['address'] ),
    			'_meta_country' => esc_attr ( $infoVideo ['country'] ),
    			'_meta_city' => esc_attr ( $infoVideo ['city'] )
    	);
    	
    	$result['_video_url'] = $infoVideo ['videourl'];
    	$result['_meta_video'] =  $infoVideo ['place_image1'];
    	$result['_meta_image'] = $infoVideo ['place_image2'];
    	$result['_meta_zipcode'] = $infoVideo ['zipcode'];
    	$result['_meta_keywords'] = $infoVideo ['keywords'];
    	$result['_meta_address'] =  $infoVideo ['address'];
    	$result['_meta_country'] = $infoVideo ['country'];
    	$result['_meta_city'] = $infoVideo ['city'];
    	
    	
    	$post_data [] = array (
    			"post_title" => $infoVideo ['place_title'],
    			"post_content" => htmlspecialchars($infoVideo ['description'] ),
    			"post_image" => $image_meta,
    			"post_category" => $infoVideo ['category'],
    			"post_tags" => $infoVideo ['place_tag'],
    			"post_meta" => $post_meta,
    			"post_author" => $user_id
    	);
    	
    	$result['post_title'] = $infoVideo ['place_title'];
    	$result['post_content'] =  $infoVideo ['description'];
    	$result['post_image'] = $image_meta;
    	$result['post_category'] = $infoVideo ['category'];
    	$result['post_tags'] =  $infoVideo ['place_tag'];
    	$result['post_meta'] = $infoVideo ['place_tag'];
    	$result['post_author'] = $user_id;
    	
    	
		for($i = 0; $i < count ( $post_data ); $i ++) {
			$post_title = $post_data [$i] ['post_title'];
			
			$stmt = $this->conn->prepare ( "SELECT count(wpP.ID) FROM wp_posts wpP where wpP.post_title like '" . $post_title . "' and post_type='video_listing' and post_status in ('publish','draft')" );
			$stmt->execute ();
			$stmt->bind_result ( $post_count );
			
			if (! $post_count) {
				$post_data_array = array ();
				$catids_arr = array ();
				$my_post = array ();
				$post_data_array = $post_data [$i];
				$my_post ['post_title'] = $post_data_array ['post_title'];
				$my_post ['post_content'] = $post_data_array ['post_content'];
				$my_post ['post_type'] = 'video_listing';
				if ($post_data_array ['post_author']) {
					$my_post ['post_author'] = $post_data_array ['post_author'];
				} else {
					$my_post ['post_author'] = 1;
				}
				$my_post ['post_status'] = 'publish';//inkthemes_get_option ( 'video_post_mode' );
				$my_post ['post_category'] = get_category_by_slug($post_data_array ['post_category']);
				$my_post ['tags_input'] = $post_data_array ['post_tags'];
				require_once '.././../wp-includes/post.php';
				
				
				$last_postid = wp_insert_post ( $my_post );
				wp_set_object_terms ( $last_postid, $post_data_array ['post_category'], $taxonomy = 'video_cat' );
				wp_set_object_terms ( $last_postid, $post_data_array ['post_tags'], $taxonomy = 'video_tag' );
				
				$post_meta = $post_data_array ['post_meta'];
				if ($post_meta) {
					foreach ( $post_meta as $mkey => $mval ) {
						update_post_meta ( $last_postid, $mkey, $mval );
					}
				}
				$post_image = $post_data_array ['post_image'];
				if ($post_image) {
					for($m = 0; $m < count ( $post_image ); $m ++) {
						$menu_order = $m + 1;
						$image_array = explode ( '/', $post_image [$m] );
						$img_name = $image_array [count ( $image_array ) - 1];
						$img_name_arr = explode ( '.', $img_name );
						$post_img = array ();
						$post_img ['post_title'] = $img_name_arr [0];
						$post_img ['post_status'] = 'attachment';
						$post_img ['post_parent'] = $last_postid;
						$post_img ['post_type'] = 'attachment';
						$post_img ['post_mime_type'] = 'image/jpeg';
						$post_img ['menu_order'] = $menu_order;
						$last_postimage_id = wp_insert_post ( $post_img );
						update_post_meta ( $last_postimage_id, '_wp_attached_file', $post_image [$m] );
						$post_attach_arr = array (
								"width" => 580,
								"height" => 480,
								"hwstring_small" => "height='100' width='100'",
								"file" => $post_image [$m] 
						);
						wp_update_attachment_metadata ( $last_postimage_id, $post_attach_arr );
					}
				}
			}
		}
		return  $result;
    }
    
    public function deleteVideo($video_id) {
    	require_once '.././../wp-includes/post.php';
    	return wp_delete_post($video_id);
    }
    
    
    public function editVideo($videoInfo) {
    	
    	$fields = array(
    			'postid',
    			'place_title',
    			'description',
    			'_video_url',
    			'_meta_image',
    			'_meta_video',
    			'place_tag'
    	);
    	//Fecth form values
    	foreach ($fields as $field) {
    		if (isset($videoInfo[$field])) {
    			$posted[$field] = stripcslashes(trim($videoInfo[$field]));
    		}
    	}
    	$posted['category'] = $videoInfo['category'];
    	$posted['tag'] = explode(',', $videoInfo['place_tag']);
    	$listing_data = array();
    	$listing_meta = array();
    	$listing_meta = array(
    			'_video_url' => $videoInfo['_video_url'],
    			'_meta_video' => $videoInfo['_meta_video'],
    			'_meta_image' => $videoInfo['_meta_image']
    	);
    	$listing_data = array(
    			"ID" => $videoInfo['postid'],
    			"post_type" => 'video_listing',
    			"post_title" => $posted['place_title'],
    			"post_status" => 'publish',//$posted['post_status'],
    			"post_content" => $posted['description'],
    			"post_category" => $posted['category'],
    			"tags_input" => $posted['tag'],
    	);
    	$last_postid = wp_update_post($listing_data);
    	wp_set_object_terms($last_postid, $posted['category'], $taxonomy = 'video_cat');
    	wp_set_object_terms($last_postid, $posted['tag'], $taxonomy = 'video_tag');
    	$post_meta = $listing_meta;
    	if ($post_meta) {
    		foreach ($post_meta as $mkey => $mval) {
    			update_post_meta($last_postid, $mkey, $mval);
    		}
    	}
    	return $last_postid;
    }
    
	public function searchByWhatWhere($que, $loc){
		global $wpdb;
		$sql = "";
		$sqlq = " (
				wp_posts.post_title like '%$que%'
				OR wp_posts.post_content like '%$que%'
				OR ( wp_postmeta.meta_key='_meta_keywords' and wp_postmeta.meta_value like '%$que%' )
				)          
		 ";
		$sqll = "
				(
				(mt1.meta_key='_meta_address' and mt1.meta_value like '%$loc%')
				OR
				(mt2.meta_key='_meta_country' and mt2.meta_value like '%$loc%')
				OR
				(mt3.meta_key='_meta_city' and mt3.meta_value like '%$loc%')
				)";
		if( $que != "" && $loc == "" ) {
			$sql = $sqlq;
		}
		else if ( $que == "" && $loc != "" ) {
			$sql = $sqll;
		}
		else if ( $que != "" && $loc != "" ) {
			$sql = $sqlq. " AND " . $sqll;
		}    
		$querystr = "
			SELECT DISTINCT wp_posts.*
			FROM wp_posts, 
				 wp_postmeta,
				 wp_postmeta as mt1,
				 wp_postmeta as mt2,
				 wp_postmeta as mt3
			WHERE wp_posts.post_type = 'video_listing' AND
				  wp_posts.post_status = 'publish' AND
				  wp_posts.ID = wp_postmeta.post_id AND
				  wp_posts.ID = mt1.post_id	AND
				  wp_posts.ID = mt2.post_id	AND
				  wp_posts.ID = mt3.post_id			
			AND 
			" . $sql . "
			ORDER BY wp_posts.post_title DESC
		";
		$searched_posts = $wpdb->get_results($querystr);
		
		$result = array();
		
		foreach($searched_posts  as $sposts) {
			$tmp = array();
			
			$tmp['author_username'] = get_the_author_meta('user_nicename', $sposts->post_author);
			$tmp ["id"] = $sposts->ID;
			$tmp ["post_author"] = $sposts->post_author;
			$tmp ["post_date"] = $sposts->post_date;
			$tmp ["post_date_gmt"] = $sposts->post_date_gmt;
			$tmp ["post_content"] = htmlspecialchars($sposts->post_content);
			$tmp ["post_title"] = htmlspecialchars($sposts->post_title);
			$tmp ["post_excerpt"] =$sposts->post_excerpt;
			$tmp ["post_status"] = $sposts->post_status;
			$tmp ["comment_status"] = $sposts->comment_status;
			$tmp ["ping_status"] = $sposts->ping_status;
			$tmp ["post_password"] = $sposts->post_password;
			$tmp ["post_name"] = htmlspecialchars($sposts->post_name);
			$tmp ["to_ping"] = $sposts->to_ping;
			$tmp ["pinged"] = $sposts->pinged;
			$tmp ["post_modified"] = $sposts->post_modified;
			$tmp ["post_modified_gmt"] = $sposts->post_modified_gmt;
			$tmp ["post_content_filtered"] = $sposts->post_content_filtered;
			$tmp ["post_parent"] = $sposts->post_parent;
			$tmp ["guid"] = $sposts->guid;
			$tmp ["menu_order"] = $sposts->menu_order;
			$tmp ["post_type"] = $sposts->post_type;
			$tmp ["post_mime_type"] = $sposts->post_mime_type;
			$tmp ["comment_count"] = $sposts->comment_count;
			
			$post_meta = array ();
			
			$post_meta_result = get_post_meta($sposts->ID);
			$post_meta['_post_title'] = htmlspecialchars($post_meta_result['_post_title'][0]);
			$post_meta['_post_content'] = htmlspecialchars($post_meta_result['_post_content'][0]);
			$post_meta['_video_url'] = $post_meta_result['_video_url'][0];
			$post_meta['_meta_video'] = $post_meta_result['_meta_video'][0];
			$post_meta['_meta_image'] = $post_meta_result['_meta_image'][0];
			$post_meta['_meta_zipcode'] = $post_meta_result['_meta_zipcode'][0];
			$post_meta['_meta_keywords'] = htmlspecialchars($post_meta_result['_meta_keywords'][0]);
			$post_meta['_meta_address'] = htmlspecialchars($post_meta_result['_meta_address'][0]);
			$post_meta['_meta_country'] = htmlspecialchars($post_meta_result['_meta_country'][0]);
			$post_meta['_meta_city'] = htmlspecialchars($post_meta_result['_meta_city'][0]);
			$post_meta['post_views_count'] = $post_meta_result['post_views_count'][0];
			$tmp ["post_meta"] =  $post_meta;
			array_push ( $result, $tmp );
		}
		return $result;
	}
	
	
    

}

?>
