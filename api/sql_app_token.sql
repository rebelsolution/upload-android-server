CREATE TABLE IF NOT EXISTS `app_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_last_login` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_login` (`user_login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=0 ;